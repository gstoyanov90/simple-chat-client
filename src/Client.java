
import java.io.*;
import java.net.Socket;
import java.util.Scanner;


public class Client {

    private String hostAddress;
    private int portNumber;
    private String userName;
    private Scanner in; //read from socket inputStream
    private PrintWriter out; //write to socket outputStream


    Client(String hostAddress, int portNumber, String userName) {
        this.hostAddress = hostAddress;
        this.portNumber = portNumber;
        this.userName = userName;
    }

    public static void main(String[] args) {

        int port = 5000;
        String host = "127.0.0.1";

        System.out.println("Pick up a username and press enter:");
        Scanner stdIn = new Scanner(System.in);
        String userName = stdIn.nextLine();

        Client client = new Client(host, port, userName);
        client.start();


        while (true) {
            String message = stdIn.nextLine();
            client.sendMessage(message);
        }
    }

    private void start() {
        try {

            Socket socket = new Socket(hostAddress, portNumber);
            System.out.println("You are connected to " + portNumber);
            in = new Scanner(socket.getInputStream());
            out = new PrintWriter(socket.getOutputStream());

            ListenForInput client = new ListenForInput();
            Thread t = new Thread(client);
            t.start();

        } catch (Exception e) {
            System.out.println("Problem with connection to server: " + e);
        }
    }

    //Send message to the server
    private synchronized void sendMessage(String msg) {
        out.println(msg);
        out.flush();
    }

    class ListenForInput implements Runnable {

        @Override
        public void run() {
            sendMessage(userName);
            try {
                while (true) {
                    if (in.hasNext()) {
                        System.out.println(in.nextLine());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}

