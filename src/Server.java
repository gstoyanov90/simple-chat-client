
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import java.util.Vector;


public class Server {
    final int PORT = 5000;
    private Vector<NewClient> clientsList = new Vector<NewClient>();
    private int uniqueID = 0;

    public static void main(String[] args) {
        Server server = new Server();
        server.go();
    }

    private void go() {

        try {
            ServerSocket serverSocket = new ServerSocket(PORT);
            System.out.println("Waiting for clients...");

            while (true) {
                Socket s = serverSocket.accept();
                NewClient chat = new NewClient(s);
                System.out.println("Client number " + chat.getId() + " connected from: " + s.getLocalAddress().getHostName());
                clientsList.add(chat);
                Thread t = new Thread(chat); //for every new client make new thread and start it.
                t.start();
            }
        } catch (Exception e) {
            System.out.println("Problem with establishing network connection: ");
            e.printStackTrace();
        }

    }

    class NewClient implements Runnable {
        private Scanner in;
        private PrintWriter out;
        private SimpleDateFormat sdf;
        private Socket socket;
        private int id;
        private String userName;

        NewClient(Socket s) {
            this.socket = s;
            this.id = ++uniqueID;
            try {
                in = new Scanner(socket.getInputStream());
                out = new PrintWriter(socket.getOutputStream());
                sdf = new SimpleDateFormat("HH:mm:ss");
            } catch (Exception e) {
                System.out.println("Exception while creating input/output streams: " + e);
            }
            setUserName();
        }


        public void setUserName() {
            String name = in.nextLine();
            if (name != null && !name.isEmpty())
                this.userName = name;
            else
                this.userName = "Anonymous " + id;
        }

        public int getId() {
            return this.id;
        }

        public String getUsername() {
            return this.userName;
        }

        @Override
        public void run() {

            try {
                while (true) {
                    while (!Thread.currentThread().isInterrupted()) {
                        if (clientsList.size() == 1) {
                            String information = "You are alone at the chat room!";
                            writeMsg("Server", information);
                        }

                        try {
                            String input = in.nextLine();
                            System.out.println(userName + " said: " + input);
                            broadcast(input);
                        } catch (Exception e) {
                            disconnectClient(true);
                        }
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        private synchronized void writeMsg(String user, String input) {  // Send message to the client

            String msg = input + " on " + sdf.format(new Date());
            out.println(user + " said: " + msg);
            out.flush();

        }

        private synchronized void broadcast(String input) {  // Handles different type of messages
            switch (input) {

                case "clients":
                    String message = "Active users now are: ";

                    for (int i = 0; i < clientsList.size(); i++) {
                        NewClient client = clientsList.get(i);

                        if (i == 0)
                            message += client.getUsername();
                        else if (i == clientsList.size() - 1)
                            message += " and " + client.getUsername();
                        else
                            message += ", " + client.getUsername();
                    }
                    writeMsg("Server", message);
                    break;

                case "disconnect":
                    disconnectClient(false);
                    break;

                default:
                    for (NewClient newClient : clientsList) {
                        newClient.writeMsg(this.userName, input); // this.username -> user that typed the last message that is received
                    }
            }

        }

        private synchronized void disconnectClient(boolean disconnected) { // check if user is already disconnected

            int position = 0;

            for (NewClient newClient : clientsList) {
                if (!newClient.getUsername().equals(this.userName)) {
                    String message = this.userName + " was disconnected from the server";
                    newClient.writeMsg("Server", message);
                } else {
                    position = clientsList.indexOf(newClient);
                }
            }
            if (!disconnected) {
                String message = "You were disconnected from the server";
                writeMsg("Server", message);
            }

            clientsList.removeElementAt(position);
            Thread.currentThread().interrupt();

            try {
                this.socket.close();
            } catch (Exception e) {
                System.out.println("Can't close socket: " + e);
            }
        }
    }
}