# README #


### Info ###

* This is simple console chat written in Java 8. My idea was to build sample client-server application to learn some basic threads and networking in java
* Version 1.0

### Set up: ###

* Just run the server 
*I-From console:
*1) javac server.java
*2) javac client.java
*3) java server 
*4) java client. 
*You could make as many clients as you wish
*II-From your fav IDE :) 

* All information about who is at the chat right now is in Vector clientsList @ Server.java